import java.util.Scanner;

public class Quadrieren {

    public static void main(String[] args) {
		title();
		double value=readInput();
        output(value,square(value));
    }
	public static void title(){
		System.out.println("Dieses Programm berechnet die Quadratzahl x^2");
		System.out.println("---------------------------------------------");
	}
	public static double readInput(){
		Scanner tastatur = new Scanner(System.in);
		System.out.println("Geben sie einen Wert für x an: ");
		return tastatur.nextDouble();
	}
	public static double square(double x){
		return x * x;
	}
	public static void output(double value, double result){
		System.out.printf("x = %.2f und x^2= %.2f\n", value, result);

	}
}
