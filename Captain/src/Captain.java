
public class Captain {

    private String surname;
    private String name;
    private int captainYears;
    private double salary;

    // 3. Fuegen Sie in der Klasse 'Captain' das Attribut 'name' hinzu und
    // implementieren Sie die entsprechenden get- und set-Methoden.
    // Implementieren Sie einen Konstruktor, der den Namen und den Vornamen
    // initialisiert.
    // Implementieren Sie einen Konstruktor, der alle Attribute
    // initialisiert.

    public Captain(String surname, String name, int captainYears, double salary) {
        this.surname = surname;
        this.name = name;
        this.captainYears = captainYears;
        this.salary = salary;
    }

    public Captain(String surname, String name) {
        this.surname = surname;
        this.name = name;
        this.captainYears = 0;
        this.salary = 0;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSurname() {
        return this.surname;
    }

    public void setSalary(double gehalt) {
        // Implementieren Sie die entsprechende set-Methode.
        // Ber�cksichtigen Sie, dass das Gehalt nicht negativ sein darf.
        if (gehalt > 0) {
            this.salary = gehalt;
        } else {
            System.out.println("Salary wasn't changed as the value you put in was negative.");
        }

    }

    public double getSalary() {
        // Implementieren Sie die entsprechende get-Methoden.
        return this.salary;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCaptainYears() {
        return captainYears;
    }

    public void setCaptainYears(int captainYears) {
        if(captainYears>=0){
            this.captainYears = captainYears;
        } else {
            System.out.println("Years weren't changed as the given value was smaller than 0");
        }
    }

    public String fullName(){
        return String.format("%s %s", this.name, this.surname);
    }

    @Override
    public String toString() {
        return "Captain{" +
                "surname='" + surname + '\'' +
                ", name='" + name + '\'' +
                ", captainYears=" + captainYears +
                ", salary=" + salary +
                '}';
    }

    // Implementieren Sie die set-Methode und die get-Methode f�r
    // captainYears.
    // Ber�cksichtigen Sie, dass das Jahr nach Christus sein soll.
    // Implementieren Sie eine Methode 'vollname', die den vollen Namen
    // (Vor- und Nachname) als string zur�ckgibt.
}