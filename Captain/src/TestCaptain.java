//import Captain;

public class TestCaptain {

    public static void main(String[] args) {
        //Erzeugen der Objekte
        Captain cap1 = new Captain("Jean Luc", "Picard", 2364, 4500.0);
        Captain cap2 = new Captain("Azetbur", "Gorkon", 2373, 0.0);
        Captain cap3 = new Captain("Tim", "Tamerthon", 1, 5.0);
        Captain cap4 = new Captain("Tom", "Tamerthon");
        Captain cap5 = new Captain("Tem", "Tamerthon");
        cap4.setSalary(10.0);
        cap5.setSalary(15.0);
        /* Erzeugen Sie ein zusaetzliches Objekt cap3 und geben Sie es auch auf der Konsole aus,
         * die Attributwerte denken Sie sich aus.
         */


        /* Erzeugen Sie zwei zusaetzliche Objekte cap4 und cap5
         * mit dem Konstruktor, der den Namen und Vornamen initialisiert,
         * die Attributwerte denken Sie sich aus.
         */


        //Setzen der Attribute
        /* Fuegen Sie cap4 und cap5 jeweils ein Gehalt hinzu,
         * die Attributwerte denken Sie sich aus.
         * Geben Sie cap4 und cap5 auch auf dem Bildschirm aus.
         */

        //Bildschirmausgabe
        System.out.println("Name: " + cap1.getSurname());
        System.out.println("Vorname: " + cap1.getName());
        System.out.println("Kapit�n seit: " + cap1.getCaptainYears());
        System.out.println("Gehalt: " + cap1.getSalary() + " F�derationsdukaten");
        System.out.println("Vollname: " + cap1.fullName());
        System.out.println(cap1 + "\n"); //Die toString() Methode wird aufgerufen
        System.out.println("\nName: " + cap2.getSurname());
        System.out.println("Vorname: " + cap2.getName());
        System.out.println("Kapit�n seit: " + cap2.getCaptainYears());
        System.out.println("Gehalt: " + cap2.getSalary() + " Darsek");
        System.out.println("Vollname: " + cap2.fullName());
        System.out.println(cap2 + "\n"); //Die toString() Methode wird aufgerufen

        //Ausgabe von cap4, erg�nzen Sie die fehlenden Methodenaufrufen
        System.out.println("Name: " + cap4.getName());
        System.out.println("Vorname: " + cap4.getName());
        System.out.println("Kapit�n seit: " + cap4.getCaptainYears());
        System.out.println("Gehalt: " + " F�derationsdukaten" + cap4.getSalary());
        System.out.println("Vollname: " + cap4.fullName());
        System.out.println(cap4 + "\n"); //Die toString() Methode wird aufgerufen

        //Ausgabe von cap5, erg�nzen Sie die fehlenden Methodenaufrufen
        System.out.println("Name: " + cap5.getName());
        System.out.println("Vorname: " + cap5.getName());
        System.out.println("Kapit�n seit: " + cap5.getCaptainYears());
        System.out.println("Gehalt: " + " F�derationsdukaten" + cap5.getSalary());
        System.out.println("Vollname: " + cap5.fullName());
        System.out.println(cap5 + "\n"); //Die toString() Methode wird aufgerufen
    }

}