import java.util.Scanner;

class Fahrkartenautomat {
    public static void main(String[] args) {
        // 5.1: Durch das verwenden von zwei Arrays erleichter man sich selbst die Datenverwaltung indem man Datensätze geschlossen übergeben kann.
        // Ich persönlich habe mich um das ganze noch weiter zu vereinfachen dazu entschieden nur einen Array gefüllt mit Objekten zu wählen. Auf diese
        // Art und Weise muss ich nur einen Array verwalten und übergeben und habe die Datensätze als Einheiten (Objekte) gespeichert.
        Fahrkarte[] alleKarten = {
                new Fahrkarte("Einzelfahrschein Berlin AB", 2.90),
                new Fahrkarte("Einzelfahrschein Berlin BC", 3.30),
                new Fahrkarte("Einzelfahrschein Berlin ABC", 3.60),
                new Fahrkarte("Kurzstrecke", 1.90),
                new Fahrkarte("Tageskarte AB", 8.60),
                new Fahrkarte("Tageskarte BC", 9.00),
                new Fahrkarte("Tageskarte ABC", 9.60),
                new Fahrkarte("Kleingruppen-Tageskarte Berlin AB", 23.50),
                new Fahrkarte("Kleingruppen-Tageskarte Berlin BC", 24.30),
                new Fahrkarte("Kleingruppen-Tageskarte Berlin ABC", 24.90),
        };
        // Ich habe meinen Arrays hier oben definiert und übergeben damit nicht bei jedem Programmdurchlauf 10 neue Objekte erschaffen werden

        Scanner tastatur = new Scanner(System.in);
        fahrkartenautomat(tastatur, alleKarten);
    }

    public static void fahrkartenautomat(Scanner tastatur, Fahrkarte[] Fahrkarten) {
        double rueckgabebetrag;


        // Geldeinwurf
        // -----------
        rueckgabebetrag = fahrkartenBezahlen(fahrkartenBestellungErfassen(tastatur, Fahrkarten), tastatur);

        // Fahrscheinausgabe
        // -----------------
        fahrkartenAusgeben();

        // Rückgeldberechnung und -Ausgabe
        // -------------------------------
        rueckgeldAusgeben(rueckgabebetrag);
        fahrkartenautomat(tastatur, Fahrkarten);
    }

    public static double fahrkartenBestellungErfassen(Scanner tastatur, Fahrkarte[] Fahrkarten) {
        System.out.println("Willkommen bei ihrem Ticketautomaten.\nHier erhältlich sind: ");
        // Indem man hier den Array verwendet statt wie vorher ein If, oder Switch-Case spart man deutlich an Code und Komplexitöt,
        // demnach auch an Möglichkeiten Fehler einzubauen.
        iteriereArray(Fahrkarten);
        int ticketNr = 10;
        while (ticketNr > 9 || ticketNr < 0) {
            System.out.println("Bitte wählen sie eine Ticketvariante");
            ticketNr = tastatur.nextInt();
            if (ticketNr < 1 || ticketNr > 3) {
                System.out.println("Ungültige Eingabe!");
            }
        }
        double zuZahlenderBetrag = Fahrkarten[ticketNr].getPreis();

        System.out.print("Anzahl der Tickets: ");
        byte ticketAnzahl = tastatur.nextByte();
        if (ticketAnzahl <= 0 || ticketAnzahl > 10) {
            ticketAnzahl = 1;
            System.out.println("Der von ihnen gewählte Wert ist ungültig, ihre Auswahl wird als 1 Ticket interpretiert!");
        }

        return zuZahlenderBetrag * ticketAnzahl;
    }

    public static void iteriereArray(Fahrkarte[] Fahrkarten) {
        for (int i = 0; i < Fahrkarten.length; i++) {
            System.out.println(i + "   " + Fahrkarten[i].getName() + "   " + Fahrkarten[i].getPreis() + "€");
        }
    }

    public static double fahrkartenBezahlen(double zuZahlenderBetrag, Scanner tastatur) {
        double eingeworfeneMuenze;
        double eingezahlterGesamtbetrag = 0.0;

        while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
            System.out.printf("Noch zu zahlen: %.2f  EURO", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
            System.out.print("\nEingabe (mind. 5Ct, höchstens 2 Euro): ");
            eingeworfeneMuenze = tastatur.nextDouble();
            eingezahlterGesamtbetrag += eingeworfeneMuenze;
        }

        return eingezahlterGesamtbetrag - zuZahlenderBetrag;
    }

    public static void fahrkartenAusgeben() {
        System.out.println("\nFahrschein wird ausgegeben");
        for (int i = 0; i < 8; i++) {
            System.out.print("=");
            warte(250);
        }
        System.out.println("\n\n");
    }

    public static void rueckgeldAusgeben(double rueckgabebetrag) {
        if (rueckgabebetrag > 0.0) {
            System.out.printf("Der Rückgabebetrag in Höhe von %.2f EURO ", rueckgabebetrag);
            System.out.println("\nwird in folgenden Münzen ausgezahlt:");

            while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
            {
                muenzeAusgeben(2, "Euro");
                rueckgabebetrag -= 2.0;
            }
            while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
            {
                muenzeAusgeben(1, "Euro");
                rueckgabebetrag -= 1.0;
            }
            while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
            {
                muenzeAusgeben(50, "Cent");
                rueckgabebetrag -= 0.5;
            }
            while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
            {
                muenzeAusgeben(20, "Cent");
                rueckgabebetrag -= 0.2;
            }
            while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
            {
                muenzeAusgeben(10, "Cent");
                rueckgabebetrag -= 0.1;
            }
            while (rueckgabebetrag >= 0.05)// 5 CENT-Münzen
            {
                muenzeAusgeben(5, "Cent");
                rueckgabebetrag -= 0.05;
            }
        }

        System.out.println("""

                Vergessen Sie nicht, den Fahrschein
                vor Fahrtantritt entwerten zu lassen!
                Wir wünschen Ihnen eine gute Fahrt.""");
        warte(1000);
    }

    public static void warte(int millisekunden) {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void muenzeAusgeben(int betrag, String einheit) {
        System.out.printf("%d %s\n", betrag, einheit);
    }
}


// 5. Ich habe Byte gewählt, da ein normaler Anwender kaum mehr als 127 Tickets am Automaten kaufen wird, für solche Großeinkäufe bieten sich die Schalter an.

// 6. durch den Ausdruck wird der Wert der Variable anzahl mit dem der Variable einzelpreis multipliziert
