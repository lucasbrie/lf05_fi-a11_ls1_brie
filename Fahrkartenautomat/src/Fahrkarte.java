public class Fahrkarte {
    private String name;
    private double preis;

    public Fahrkarte(String neuerName, double neuerPreis ){
        this.name=neuerName;
        this.preis=neuerPreis;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPreis(float preis) {
        this.preis = preis;
    }

    public double getPreis() {
        return this.preis;
    }

    public String getName() {
        return this.name;
    }
}
