public class SummeEinsBisHundert {

    public static void main(String[] args) {
        int count = 0;
        int number = 0;

        while (count <= 100) {
            number += count;
            count++;
        }
        System.out.println("Vorprüfende Schleife: " + number);

        count = 0;
        number = 0;
        do {
            number += count;
            count++;
        } while (count <= 100);
        System.out.println("Nachprüfende Schleife: " + number);

        number = 0;
        for (int iterator = 0; iterator <= 100; iterator++) {
            number += iterator;
        }
        System.out.println("Zählschleife: " + number);

    }
}
