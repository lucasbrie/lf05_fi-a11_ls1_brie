import java.util.Scanner;

public class Steuersatz {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        String selection="";
        double valuePostTaxes;
        System.out.println("Geben sie den Nettobetrag ihres Produktes ein: ");
        double valuePreTaxes=myScanner.nextDouble();
        System.out.println("Geben sie n ein wenn sie den vollen Steuersatz bezahlen werden, j falls sie den ermäßigten Steuersatz bezahlen werden.");
        while(!selection.equals("j") && !selection.equals("n")){
            selection= myScanner.next();
            if(!selection.equals("j") && !selection.equals("n")){
                System.out.println("Das war keine der angebotenen Antwortmöglichkeiten");
            }
        }
        if(selection.equals("j")) {
            valuePostTaxes=valuePreTaxes+(valuePreTaxes*0.07);
            System.out.printf("Der Bruttobetrag liegt bei %f", valuePostTaxes);
        }
        if(selection.equals("n")) {
            valuePostTaxes=valuePreTaxes+(valuePreTaxes*0.19);
            System.out.printf("Der Bruttobetrag liegt bei %f", valuePostTaxes);
        }
    }
}
