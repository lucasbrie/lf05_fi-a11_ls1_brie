import java.util.Scanner;

public class BMI {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        String selection = "";
        System.out.println("Willkommen beim BMI-Rechner, sind sie männlich oder weiblich?(m/w)");
        while (!selection.equals("m") && !selection.equals("w")) {
            selection = myScanner.next();
            if (!selection.equals("m") && !selection.equals("w")) {
                System.out.println("Das war keine der angebotenen Antwortmöglichkeiten");
            }
        }
        if (selection.equals("m")) {
            BMICalc(20, 25);
        } else {
            BMICalc(19, 24);
        }
    }

    public static void BMICalc(int underweight, int overweight) {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Geben sie ihr Gewicht (in kg) an: ");
        double weight = myScanner.nextDouble();
        System.out.println("Geben sie ihre Größe (in cm) an: ");
        double height = myScanner.nextDouble();
        height=height/100;
        double bmi = weight / (height * height);
        if (bmi < underweight) {
            System.out.printf("Sie haben einen BMI von %.2f und befinden sich damit im untergewichtigen Bereich", bmi);
        } else if (bmi > overweight) {
            System.out.printf("Sie haben einen BMI von %.2f und befinden sich damit im übergewichtigen Bereich", bmi);
        } else {
            System.out.printf("Sie haben einen BMI von %.2f und befinden sich damit im normalgewichtigen Bereich", bmi);
        }
    }
}
