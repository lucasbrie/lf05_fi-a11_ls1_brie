import java.util.Scanner;

public class vergleichZahlen {
    public static void main(String[] args) {
        compareTwoNumbers();
        compareThreeNumbers();
    }

    public static void compareTwoNumbers() {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Geben sie eine natürliche Zahl (1,2,3...) zum vergleichen ein");
        int numberOne = myScanner.nextInt();
        System.out.println("Geben sie eine zweite natürliche Zahl (1,2,3...) zum vergleichen ein");
        int numberTwo = myScanner.nextInt();

        if (numberOne == numberTwo) {
            System.out.println("Die Werte 1 und 2 sind gleichgroß!");
        }
        if (numberTwo > numberOne) {
            System.out.println("Der zweite Wert ist größer als der erste Wert!");
        }
        if (numberOne >= numberTwo) { // Warum? "==" wurde bereits evaluiert, es hier eneut zu tun sorgt für neue Ausgaben ohne tatsächlich sinnvollen Inhalt.
            System.out.println("Der erste Wert ist größer oder gleichgroß wie der zweite Wert!");
        } else {
            System.out.println("Wert 2 ist immernoch größer als Wert 1!"); // Dieser Case macht aufgrund der if Bedingung auch keinen großen Sinn, wir springen nicht nach jedem Durchlauf hier rein für eine Verabschiedung o.Ä., und andere Cases sind bereits evaluiert, für die Fehlerbehandlung ist das auch Sinnfrei weil wir hier reinspringen falls NR1 > NR2 ist
        }
    }

    public static void compareThreeNumbers() {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Geben sie eine natürliche Zahl (1,2,3...) zum vergleichen ein");
        int numberOne = myScanner.nextInt();
        System.out.println("Geben sie eine zweite natürliche Zahl (1,2,3...) zum vergleichen ein");
        int numberTwo = myScanner.nextInt();
        System.out.println("Geben sie eine dritte natürliche Zahl (1,2,3...) zum vergleichen ein");
        int numberThree = myScanner.nextInt();

        if ((numberOne > numberTwo) && (numberOne > numberThree)) {
            System.out.println("Wert 1 ist der Größte!");
        }
        if ((numberThree > numberOne) || (numberThree > numberTwo)) {
            System.out.println("Wert 3 ist mindestens der Zweitgrößte!");
        }
        if ((numberOne > numberTwo) && (numberOne > numberThree)) {
            System.out.printf("Die größte Zahl ist %d", numberOne);
        } else {
            if ((numberTwo > numberOne) && (numberTwo > numberThree)) {
                System.out.printf("Die größte Zahl ist %d", numberTwo);
            } else {
                if ((numberThree > numberOne) && (numberThree > numberTwo)) {
                    System.out.printf("Die größte Zahl ist %d", numberThree);
                } else {
                    System.out.println("Mindestens zwei der Werte sind gleichgroß!");
                }
            }
        }
    }
}
