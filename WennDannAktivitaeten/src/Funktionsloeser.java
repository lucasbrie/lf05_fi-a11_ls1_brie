import java.util.Scanner;

public class Funktionsloeser {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Geben sie einen Wert für X ein");
        double x = myScanner.nextDouble();
        if (x <= 0) {
            x = Math.pow(2.718, x);
            System.out.printf("Dieser Wert befindet sich im exponentiellen Bereich und ergibt den Funktionswert %.2f", x);
        } else if (x > 0 && x <= 3) {
            x = (x * x) + 1;
            System.out.printf("Dieser Wert befindet sich im quadratischen Bereich und ergibt den Funktionswert %.2f", x);
        } else {
            x=2*x+4;
            System.out.printf("Dieser Wert befindet sich im linearen Bereich und ergibt den Funktionswert %.2f", x);
        }
    }
}
