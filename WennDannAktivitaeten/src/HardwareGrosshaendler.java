import java.util.Scanner;

public class HardwareGrosshaendler {
    public static void main(String[] args) {
        calculateBill();
        calculateDiscount();
    }

    public static void calculateBill() {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Willkommen in ihrem Rechnungsprogramm, wie viele Mäuse haben sie verkauft?");
        int productsSold = myScanner.nextInt();
        System.out.println("Wie hoch ist der Stückpreis der Mäuse?");
        double price = myScanner.nextDouble();
        if (productsSold > 10) {
            System.out.printf("Der Preis beträgt: %.2f€", (((double) productsSold * price) * 1.19));
        } else {
            System.out.printf("Der Preis beträgt: %.2f€", (((double) productsSold * price + 10.00) * 1.19));
        }
    }

    public static void calculateDiscount() {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Wie hoch lag der Bestellwert? (in €)");
        double orderPrice = myScanner.nextDouble();

        if (orderPrice < 100) {
            System.out.printf("Der Endpreis liegt bei %.2f€", ((orderPrice * 0.9) * 1.19));
        } else if (orderPrice < 500) {
            System.out.printf("Der Endpreis liegt bei %.2f€", ((orderPrice * 0.85) * 1.19));
        } else {
            System.out.printf("Der Endpreis liegt bei %.2f€", ((orderPrice * 0.8) * 1.19));
        }
    }
}
