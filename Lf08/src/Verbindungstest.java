import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Verbindungstest {
    public static void main(String[] args) {
        try {
            // Parameter für Verbindungsaufbau definieren
            String driver = "com.mysql.jdbc.Driver";
            String url = "jdbc:mysql://localhost/eGameDarling?";
            String user = "root";
            String password = "";
            // JDBC-Treiber laden
            Class.forName(driver);
            // Verbindung aufbauen
            Connection con;
            con = DriverManager.getConnection(url, user, password);
            // SQL-Anweisungen ausführen
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM T_Accounts");

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter the Id of an Account whose password you want to change");
            int id = Integer.parseInt(reader.readLine());
            System.out.println("Enter the new password");
            String newPassword = reader.readLine();


            // Ergebnis abfragen
            while (rs.next()) {
                System.out.println(rs.getString("nickname"));
            }

            String sqlStatement = String.format("UPDATE T_Accounts SET passwort='%s' WHERE  p_account_id = %2d", newPassword, id);

            st.executeUpdate(sqlStatement);

            // Verbindung schließen
            con.close();
        } catch (Exception ex) { // Fehler abfangen
            ex.printStackTrace();// Fehlermeldung ausgeben
        }
    }
}
