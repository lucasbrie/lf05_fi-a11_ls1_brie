import java.util.Scanner;

public class Zaehler {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Geben sie eine Zahl ein:");
        int value = myScanner.nextInt();
        System.out.println("Wollen sie hoch oder runter zählen? (h/r)");
        String descAsc = myScanner.next();
        while (!descAsc.contains("h") && !descAsc.contains("r")) {
            descAsc = myScanner.next();
        }
        int counter = 0;
        if (descAsc.contains("h")) {
            do {
                counter++;
                System.out.println(counter);
            } while (counter != value);
        } else {
            counter=value;
            do {
                System.out.println(counter);
                counter--;
            } while (counter != 0);
        }
    }
}
