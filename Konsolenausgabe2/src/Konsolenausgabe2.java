public class Konsolenausgabe {

	public static void main(String[] args) {
		// 	Aufgabe 1
		String star = "*";

		System.out.printf("%4s%s\n", star, star);
		System.out.printf("%1s%7s\n", star, star);
		System.out.printf("%1s%7s\n", star, star);
		System.out.printf("%4s%s\n", star, star);

		// Aufgabe 2
		String empty = "";
		String StartValueZero = "0!";
		String StartValueOne = "1!";
		String StartValueTwo = "2!";
		String StartValueThree = "3!";
		String StartValueFour = "4!";
		String StartValueFive = "5!";
		String eq = "=";
		String one = "1";
		String two = "1 * 2";
		String three = "1 * 2 * 3";
		String four = "1 * 2 * 3 * 4";
		String five = "1 * 2 * 3 * 3 * 5";
		int intTwo = 2;
		int six = 6;
		int twentyfour = 24;
		int onehundredandtwenty = 120;

		System.out.printf("%-5s %s %-19s %s %-4s\n",	StartValueZero, eq, empty, eq, one);
		System.out.printf("%-5s %s %-19s %s %-4s\n",	StartValueOne, eq, one, eq, one);
		System.out.printf("%-5s %s %-19s %s %-4d\n",	StartValueTwo, eq, two, eq, intTwo);
		System.out.printf("%-5s %s %-19s %s %-4d\n",	StartValueThree, eq, three, eq, six);
		System.out.printf("%-5s %s %-19s %s %-4d\n",	StartValueFour, eq, four, eq, twentyfour);
		System.out.printf("%-5s %s %-19s %s %-4d\n\n",	StartValueFive, eq, five, eq, onehundredandtwenty);

		// Aufgabe 3
		String Header = "Fahrenheit  |  Celsius";
		String line = "-----------------------\n";
		double FValueOne = -20;
		double FValueTwo = -10;
		double FValueThree = 0;
		double FValueFour = 20;
		double FValueFive = 30;
		double CValueOne = -28.8889;
		double CValueTwo = -23.333;
		double CValueThree = -17.7778;
		double CValueFour = -6.6667;
		double CValueFive = -1.1111;

		System.out.printf("%s\n%s", Header, line);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueOne, CValueOne);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueTwo, CValueTwo);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueThree, CValueThree);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueFour, CValueFour);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueFive, CValueFive);
	}
}

