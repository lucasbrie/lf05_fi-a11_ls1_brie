public class Konsolenausgabe {

	public static void main(String[] args) {
		String Header = "Fahrenheit  |  Celsius";
		String line = "-----------------------\n";
		double FValueOne = -20;
		double FValueTwo = -10;
		double FValueThree = 0;
		double FValueFour = 20;
		double FValueFive = 30;
		double CValueOne = -28.8889;
		double CValueTwo = -23.333;
		double CValueThree = -17.7778;
		double CValueFour = -6.6667;
		double CValueFive = -1.1111;

		System.out.printf("%s\n%s", Header, line);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueOne, CValueOne);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueTwo, CValueTwo);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueThree, CValueThree);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueFour, CValueFour);
		System.out.printf("%+-10.0f  | %+10.2f\n", FValueFive, CValueFive);
	}
}

