public class Konsolenausgabe {

	public static void main(String[] args) {
		// 	Aufgabe 1
		System.out.println("Das ist \"ein\" Beispielsatz.");
		System.out.print("Ein Beispielsatz" + "ist das.\n\n\n");
		/* Der Unterschied zwischen println und print besteht darin, dass println nach der Ausgabe von selbst in die nächste Zeile springt.

			Aufgabe 2 */
		System.out.print("      *      \n     ***     \n    *****    \n   *******   \n  *********  \n *********** \n*************\n     ***     \n     ***     \n\n\n");

		// 	Aufgabe 3
		double numberOne = 22.4234234;	//to 22,42
		double numberTwo = 111.2222;		//to 111,22
		double numberThree = 4.0;		//to 4,00
		double numberFour = 1000000.551;	//to 1000000,55
		double numberFive = 97.34;		//to 97,34

		System.out.printf("%.2f\n", numberOne);
		System.out.printf("%.2f\n", numberTwo);
		System.out.printf("%.2f\n", numberThree);
		System.out.printf("%.2f\n", numberFour);
		System.out.printf("%.2f\n", numberFive);
	}

}
