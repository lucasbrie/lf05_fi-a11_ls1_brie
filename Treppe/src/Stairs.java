import java.util.Scanner;

public class Stairs {
    public static void main(String[] args) {
        stairsHeightAndLength();
    }

    public static void stairsHeightAndLength() {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Wie hoch soll die Treppe gebaut werden?");
        int h = myScanner.nextInt();
        System.out.println("Wie breit sollen ihre Stufen sein?");
        int b = myScanner.nextInt();

        for (int i = 1; i != h + 1; i++) {
            int maxWidth = h * b;
            if ((i * b) < maxWidth) {
                for (int iterator2 = maxWidth - i * b; iterator2 > 0; iterator2--) {
                    System.out.print(" ");
                }
            }
            for (int iterator = i * b; iterator > 0; iterator--) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
        myScanner.close();
    }

    public static void stairsHeightOnly() {
        Scanner myScanner = new Scanner(System.in);

        System.out.println("Wie hoch soll die Treppe gebaut werden?");
        int h = myScanner.nextInt();
        for (int i = 0; i < h; i++) {
            for (int iterator = i; iterator >= 0; iterator--) {
                System.out.print("*");
            }
            System.out.print("\n");
        }
        myScanner.close();
    }
}
