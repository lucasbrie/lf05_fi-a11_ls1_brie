import java.util.Scanner;

public class PCHaendler {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);

        // Benutzereingaben lesen
        String artikel = liesString("Was möchten Sie bestellen?", myScanner);
        int anzahl = liesInt("Geben Sie die Anzahl ein:", myScanner);
        double preis = liesDouble("Geben Sie den Nettopreis ein:", myScanner);
        double mwst = liesDouble("Geben Sie den Mehrwertsteuersatz in Prozent ein:", myScanner);

        // Verarbeiten
        double nettoGesamtPreis = berechneGesamtNettoPreis(anzahl, preis);
        double bruttoGesamtPreis = berechneGesamtBruttoPreis(nettoGesamtPreis, mwst);

        // Ausgeben
        rechnungAusgeben(artikel, anzahl, nettoGesamtPreis, bruttoGesamtPreis, mwst);
    }

    public static String liesString(String text, Scanner myScanner) {
        System.out.println(text);
        return myScanner.nextLine();
    }

    public static int liesInt(String text, Scanner myScanner) {
        System.out.println(text);
        return myScanner.nextInt();
    }

    public static double liesDouble(String text, Scanner myScanner) {
        System.out.println(text);
        return myScanner.nextDouble();
    }

    public static double berechneGesamtNettoPreis(int anzahl, double preis) {
        return anzahl * preis;
    }

    public static double berechneGesamtBruttoPreis(double nettoGesamtPreis, double mwst){
        return (nettoGesamtPreis * (1 + mwst / 100));
    }

    public static void rechnungAusgeben(String artikel, int anzahl, double nettoGesamtPreis, double bruttoGesamtPreis, double mwst){
        System.out.println("\tRechnung");
        System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettoGesamtPreis);
        System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttoGesamtPreis, mwst, "%");
    }
}
