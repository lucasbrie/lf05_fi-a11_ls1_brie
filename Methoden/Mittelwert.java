import java.util.Scanner;

public class Mittelwert {

    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        String input = "";
        double allValues = 0;
        int iterator = 0;
        while (!input.equals("e")) {
            System.out.println("Geben sie eine Zahl ein welche für die Mittelwertberechnung miteinbezogen werden soll (oder e um das Ergebnis zu bekommen): ");
            input = myScanner.next();
            if (!input.equals("e")) {
                allValues += (Double.parseDouble(input));

            }
            iterator++;
        }

        System.out.printf("Der Mittelwert der eingegebenen Werte is %.2f\n", averageMultipleValues(allValues, iterator));
    }

    public static double average(double x, double y, int iterator) {
        return ((x + y) / 2);
    }

    public static double averageMultipleValues(double allValues, int iterator) {
        return (allValues / iterator);
    }

}
