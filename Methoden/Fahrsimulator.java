import java.util.Scanner;

public class Fahrsimulator {

    public static void main(String[] args) {
        Scanner tastatur = new Scanner(System.in);
        double v = 0;
        v=beschleunige(v,tastatur);

        while(v!=0){
        v=beschleunige(v,tastatur);
        }
    }

    public static double beschleunige(double v, Scanner tastatur) {
        System.out.println("Beschleunige!");
        double dv=tastatur.nextDouble();

        v = v + dv;
        if (v > 130) {
            v = 130;
        } else if (v < 0) {
            v = 0;
            System.out.println("Du hast angehalten");
            return v;
        }
        System.out.printf("Du fährst %.2f km/h\n", v);
        return v;
    }
}
