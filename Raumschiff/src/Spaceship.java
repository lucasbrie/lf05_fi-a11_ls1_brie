import java.util.ArrayList;
import java.util.Random;

public class Spaceship {
    private String name;
    private int droids;
    private int photonTorpedoAmount;
    private int energyPercentage;
    private int shieldPercentage;
    private int hullPercentage;
    private int lifeSupportSystemsPercentage;
    private static ArrayList<String> broadcastCommunicator = new ArrayList<>();
    private ArrayList<Cargo> freight;

    public Spaceship() {
        this.name = "unknown";
        this.photonTorpedoAmount = 0;
        this.droids = 0;
        this.energyPercentage = 100;
        this.shieldPercentage = 100;
        this.hullPercentage = 100;
        this.lifeSupportSystemsPercentage = 100;
        this.freight = new ArrayList<>();
    }

    public Spaceship(String name, int droids, int photonTorpedoAmount, int energyPercentage, int shieldPercentage, int hullPercentage, int lifeSupportSystemsPercentage) {
        this.name = name;
        this.droids = droids;
        this.photonTorpedoAmount = photonTorpedoAmount;
        this.energyPercentage = energyPercentage;
        this.shieldPercentage = shieldPercentage;
        this.hullPercentage = hullPercentage;
        this.lifeSupportSystemsPercentage = lifeSupportSystemsPercentage;
        this.freight = new ArrayList<>();
    }

    public int getPhotonTorpedoAmount() {
        return photonTorpedoAmount;
    }

    public void setPhotonTorpedoAmount(int photonTorpedoAmount) {
        this.photonTorpedoAmount = photonTorpedoAmount;
    }

    public int getEnergyPercentage() {
        return energyPercentage;
    }

    public void setEnergyPercentage(int energyPercentage) {
        this.energyPercentage = energyPercentage;
    }

    public int getShieldPercentage() {
        return shieldPercentage;
    }

    public void setShieldPercentage(int shieldPercentage) {
        this.shieldPercentage = shieldPercentage;
    }

    public int getHullPercentage() {
        return hullPercentage;
    }

    public void setHullPercentage(int hullPercentage) {
        this.hullPercentage = hullPercentage;
    }

    public int getLifeSupportSystemsPercentage() {
        return lifeSupportSystemsPercentage;
    }

    public void setLifeSupportSystemsPercentage(int lifeSupportSystemsPercentage) {
        this.lifeSupportSystemsPercentage = lifeSupportSystemsPercentage;
    }

    public int getDroids() {
        return droids;
    }

    public void setDroids(int droids) {
        this.droids = droids;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static void setBroadcastCommunicator(ArrayList<String> broadcastCommunicator) {
        Spaceship.broadcastCommunicator = broadcastCommunicator;
    }

    // Logbuch Einträge zurückgeben
    public ArrayList<String> getBroadcastCommunicator() {
        return broadcastCommunicator;
    }

    public ArrayList<Cargo> getFreight() {
        return freight;
    }

    public void addFreight(Cargo freight) {
        this.freight.add(freight);
    }

    // Zustand des Raumschiffes
    public void statusReport() {
        System.out.println("Name: " + name);
        System.out.println("Anzahl der Droiden: " + droids);
        System.out.println("Anzahl der Photonentorpedos: " + photonTorpedoAmount);
        System.out.println("Energie ist bei " + energyPercentage + "%");
        System.out.println("Schilde sind bei " + shieldPercentage + "%");
        System.out.println("Hülle ist bei " + hullPercentage + "%");
        System.out.println("Lebenserhaltungssysteme sind bei " + lifeSupportSystemsPercentage + "%");
        System.out.println("Schiffsladung: ");
        freight.forEach(x -> System.out.println("\t" + x));
    }

    // Ladungsverzeichnis ausgeben
    public void freightLog() {
        System.out.println("Name\t|\tMenge");
        System.out.println("-----------------");
        this.freight.forEach(x -> System.out.println(x.getName() + "\t|\t" + x.getAmount()));
    }

    // Photonentorpedos abfeuern
    public void launchPhotontorpedo(Spaceship target) {
        if (photonTorpedoAmount == 0) {
            messageToAll("-=*Click*=-");
            return;
        }
        photonTorpedoAmount--;
        messageToAll("Photonentorpedo abgeschossen");
        target.gotHit();
    }

    // Phaserkanonen abfeuern
    public void firePhasercanons(Spaceship target) {
        if (energyPercentage < 50) {
            messageToAll("-=*Click*=-");
            return;
        }
        energyPercentage = energyPercentage - 50;
        messageToAll("Phaserkanone abgeschossen");
        target.gotHit();
    }

    // Nachricht an Alle
    public void messageToAll(String message) {
        Spaceship.broadcastCommunicator.add(message);
    }

    // Treffer vermerken
    public void gotHit() {
        messageToAll(name + " wurde getroffen!");

        // Treffer vermerken 2
        if (shieldPercentage > 0) {
            shieldPercentage = shieldPercentage - 50;
        }
        if (shieldPercentage == 0) {
            hullPercentage = hullPercentage - 50;
            energyPercentage = energyPercentage - 50;
        }
        if (hullPercentage == 0) {
            lifeSupportSystemsPercentage = 0;
            messageToAll("Lebenserhaltungssysteme von " + name + " wurden zerstört");
        }
    }

    //Logbuch Einträge zurückgeben alternative?
    public void showLog() {
        broadcastCommunicator.forEach(System.out::println);
    }

    // Ladung Photonentorpedos einsetzen
    public void loadPhotontorpedosFromFreight(int amount) {
        freight.forEach(x -> {
            if (x.getName().equals("Photonentorpedo")) {
                if (amount > x.getAmount()) {
                    photonTorpedoAmount = photonTorpedoAmount + x.getAmount();
                    System.out.println(x.getAmount() + " Photonentorpedo(s) eingesetzt");
                    x.setAmount(0);
                } else {
                    photonTorpedoAmount = photonTorpedoAmount + amount;
                    x.setAmount(x.getAmount() - amount);
                    System.out.println(amount + " Photonentorpedo(s) eingesetzt");
                }
            }
        });
        cleanUpFreight();
        if (photonTorpedoAmount == 0) {
            System.out.println("Keine Photonentorpedos gefunden!");
            messageToAll("-=*Click*=-");
        }

    }

    // Ladungsverzeichnis aufräumen
    public void cleanUpFreight() {
        freight.removeIf(item -> item.getAmount() == 0);
    }

    // Reperaturdroiden einsetzen
    public void repair(boolean repairShields, boolean repairEnergy, boolean repairHull, int droids) {
        Random r = new Random();
        int low = 0;
        int high = 101;
        int result = r.nextInt(high - low) + low;

        int a = repairShields ? 1 : 0;
        int b = repairEnergy ? 1 : 0;
        int c = repairHull ? 1 : 0;
        int amountOfStructuresToRepair = a + b + c;

        if (droids > this.droids) {
            droids = this.droids;
        }

        int repairAmount = (result * droids) / amountOfStructuresToRepair;

        if (repairShields) {
            shieldPercentage = shieldPercentage + repairAmount;
            if (shieldPercentage > 100) {
                shieldPercentage = 100;
            }
        }
        if (repairEnergy) {
            energyPercentage = energyPercentage + repairAmount;
            if (energyPercentage > 100) {
                energyPercentage = 100;
            }
        }
        if (repairHull) {
            hullPercentage = hullPercentage + repairAmount;
            if (hullPercentage > 100) {
                hullPercentage = 100;
            }
        }
    }

    public static void showBroadCastCommunicator(){
        System.out.println("Inhalte des Broadcast Kommunikators: ");
        broadcastCommunicator.forEach(x -> System.out.println("\t" + x));
    }

}
