public class Main {
    public static void main(String[] args) {
        // Anfangszustand
        Spaceship klingonen = new Spaceship("IKS Hegh'ta", 2, 1, 100, 100, 100, 100);
        Spaceship romulaner = new Spaceship("IRW Khazara", 2, 2, 100, 100, 100, 100);
        Spaceship vulkanier = new Spaceship("Ni'Var", 5, 0, 80, 80, 50, 100);

        Cargo l1 = new Cargo("Ferengi Schneckensaft", 200);
        Cargo l2 = new Cargo("Bat'leth Klingonen Schwert", 200);
        Cargo l3 = new Cargo("Borg-Schrott", 5);
        Cargo l4 = new Cargo("Rote Materie", 5);
        Cargo l5 = new Cargo("Plasma-Waffe", 50);
        Cargo l6 = new Cargo("Forschungssonde", 35);
        Cargo l7 = new Cargo("Photonentorpedo", 3);

        klingonen.addFreight(l1);
        klingonen.addFreight(l2);
        romulaner.addFreight(l3);
        romulaner.addFreight(l4);
        romulaner.addFreight(l5);
        vulkanier.addFreight(l6);
        vulkanier.addFreight(l7);

        klingonen.launchPhotontorpedo(romulaner);
        romulaner.firePhasercanons(klingonen);
        vulkanier.messageToAll("Gewalt ist nicht logisch");
        // Der Status beinhaltet bereits das Ladungsverzeichnis,
        // falls dieses seperat ausgegeben werden soll
        // muss klingonen.freightLog() aufgerufen werden.
        klingonen.statusReport();
        vulkanier.repair(true, true, true, 5);
        vulkanier.loadPhotontorpedosFromFreight(3);
        // Die Funktion zum aufräumen des Ladunsgverzeichnisses wird schon von
        // loadPhotontopedosFromFreight aufgerufen, könnte aber auch seperat mit
        // vulkanier.cleanUpFreight() aufgerufen werden.
        klingonen.launchPhotontorpedo(romulaner);
        klingonen.launchPhotontorpedo(romulaner);
        klingonen.statusReport();
        romulaner.statusReport();
        vulkanier.statusReport();
        Spaceship.showBroadCastCommunicator();
    }
}
