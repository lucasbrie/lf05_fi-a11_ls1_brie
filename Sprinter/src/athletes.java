import java.util.Scanner;

public class athletes {
    public static void main(String[] args) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Willkommen zum Prognoseprogramm für den diesjährigen 1000m-Lauf der Topathleten. Bitte geben sie die Durchschnittsgeschwindigkeit des erste Läufers an: ");
        double athleteOne = myScanner.nextDouble();
        System.out.println("Bitte geben sie die Geschwindigkeit des zweiten Läufers an: ");
        double athleteTwo = myScanner.nextDouble();
        double distanceOne = 0;
        double distanceTwo = 0;
        int time = 0;
        int slowerOne = 0;

        if (athleteOne < athleteTwo) {
            System.out.println("Da Nr. 1 langsamer ist als Nr. 2 bekommt er einen Vorsprung von 250m");
            slowerOne = 1;
        }
        if (athleteTwo < athleteOne) {
            System.out.println("Da Nr. 1 langsamer ist als Nr. 2 bekommt er einen Vorsprung von 250m");
            slowerOne = 2;
        }
        warte(500);
        while (distanceOne < 1000 && distanceTwo < 1000) {
            time++;
            distanceOne = athleteOne * time;
            distanceTwo = athleteTwo * time;

            if (slowerOne == 1) {
                distanceOne = 250 + athleteOne * time;
            }
            if (slowerOne == 2) {
                distanceTwo = 250 + athleteTwo * time;
            }

            if (distanceOne < 1000 && distanceTwo < 1000) {
                System.out.println("Nr. 1 ist: " + distanceOne + "m gelaufen,\nNr. 2 ist: " + distanceTwo + "m gelaufen!");
            } else if (distanceOne > 1000 || distanceTwo > 1000) {
                if (distanceOne > distanceTwo) {
                    System.out.println("Nr. 1 gewinnt!");
                } else if (distanceOne == distanceTwo) {
                    System.out.println("Gleichstand!");
                } else {
                    System.out.println("Nr. 2 gewinnt!");
                }
            }
            warte(500);
        }
    }

    public static void warte(int millisekunden) {
        try {
            Thread.sleep(millisekunden);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
