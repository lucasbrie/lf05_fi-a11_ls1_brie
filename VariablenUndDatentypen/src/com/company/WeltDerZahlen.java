package com.company;

public class WeltDerZahlen {

    public static void main(String[] args) {

        // Im Internet gefunden ?
        // Die Anzahl der Planeten in unserem Sonnesystem
        byte anzahlPlaneten = 8 ;

        // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 100000000000000L;

        // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 364500000;

        // Wie alt bist du?  Wie viele Tage sind das?
        short alterTage = 7665;

        // Wie viel wiegt das schwerste Tier der Welt?
        // Schreiben Sie das Gewicht in Kilogramm auf!
        int gewichtKilogramm = 150000;

        // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
        long flaecheGroessteLand =17098242L;

        // Wie groß ist das kleinste Land der Erde?

        float flaecheKleinsteLand =0.44f;

    /*  *********************************************************

         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen

    *********************************************************** */

        System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);

        System.out.println("Anzahl der Sterne: " + anzahlSterne);

        System.out.println("Anzahl der Bewohner Berlins: " + bewohnerBerlin);

        System.out.println("Ich bin " + alterTage + " Tage alt");

        System.out.println("Ein Blauwal wiegt: " + gewichtKilogramm);

        System.out.println("Russland ist: " + flaecheGroessteLand + " Quadratkilometer groß");

        System.out.println("Der Vatikan ist: " + flaecheKleinsteLand + " Quadratkilometer groß");
        
        System.out.println(" *******  Ende des Programms  ******* ");
    }
}
