package com.company;

import java.util.Scanner;

public class Konsoleneingabe {

    public static void main(String[] args) {
        Scanner reader=new Scanner(System.in);
        String nameInput;
        int ageInput;
        float heightInput;
        char genderInput;

        System.out.println("Geben sie ihren Namen ein: ");
        nameInput = reader.next();
        System.out.println("Geben sie ihr Alter ein: ");
        ageInput = reader.nextInt();
        System.out.println("Geben sie ihre Größe(cm) an: ");
        heightInput=reader.nextFloat();
        System.out.println("Geben sie ihr Geschlecht an: ");
        genderInput=reader.next().charAt(0);

        System.out.println("Sie heißen "+nameInput+".");
        System.out.println("Sie sind "+ageInput+" Jahre alt.");
        System.out.println("Und sie sind " +heightInput+"cm groß.");
        System.out.println("Ihr Geschlecht ist: "+genderInput);

    }
}
