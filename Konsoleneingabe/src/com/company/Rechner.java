package com.company;

import java.util.Scanner;

public class Rechner {

    public static void main(String[] args) // Hier startet das Programm
    {

        // Neues Scanner-Objekt myScanner wird erstellt
        Scanner myScanner = new Scanner(System.in);

        System.out.print("Bitte geben Sie eine Zahl ein: ");

        // Die Variable zahl1 speichert die erste Eingabe
        float zahl1 = myScanner.nextFloat();

        System.out.print("Bitte geben Sie eine zweite Zahl ein: ");

        // Die Variable zahl2 speichert die zweite Eingabe
        float zahl2 = myScanner.nextFloat();

        // Die Addition der Variablen zahl1 und zahl2
        // wird der Variable ergebnis zugewiesen.
        float ergebnis;

        ergebnis = zahl1 + zahl2;
        System.out.println("\n\n\nDas Ergebnis der Addition lautet: ");
        System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);

        ergebnis = zahl1 - zahl2;
        System.out.println("\nDas Ergebnis der Subtraktion lautet: ");
        System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnis);

        ergebnis = zahl1 * zahl2;
        System.out.println("\nDas Ergebnis der Multiplikation lautet: ");
        System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnis);

        ergebnis = zahl1 / zahl2;
        System.out.println("\nDas Ergebnis der Division lautet: ");
        System.out.print(zahl1 + " / " + zahl2 + " = " + ergebnis);

        myScanner.close();

    }
}
