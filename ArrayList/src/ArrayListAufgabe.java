import java.util.ArrayList;
import java.util.Scanner;

public class ArrayListAufgabe {

    public static void main(String[] args) {

        // TODO: 1. Erzeugen Sie zwei Listen vom Typ int.
        // In einer werden die Zufallszahlen hinzugef�gt
        // und in der andere werden die Indices an der
        // die gesuchte Zahl sich befindet gespeichert.


        // Bekomme Warnings wegen unnötigem Typing bei ArrayList<Integer> myList = new ArrayList<Integer>();
        ArrayList<Integer> myList = new ArrayList<>();
        ArrayList<Integer> myIndices = new ArrayList<>();

        Scanner ms = new Scanner(System.in);

        // TODO: 2.: Deklaration von zwei Variablen f�r die beliebige Suchzahl und
        // um die H�ufigkeit dieser Zahl in der Liste zu speichern.
        int zahl, zaehleZahl = 0;

        // TODO: 3. Erg�nzen Sie die Z�hlschleife. Diese soll die 20
        // zuf�llig erzeugten Zahlen in der Liste hinzuf�gen.

        for (int i = 19; i >= 0; i--) {
            myList.add((int) (Math.random() * 8 + 1));
        }

        // TODO: 4. Geben Sie die Liste in der Konsole aus.
        for (int i = 19; i >= 0; i--) {
            System.out.printf("myList %2d : %3d\n", i, myList.get(i));
        }

        // TODO: 5. Eingabeaufforderung einer Zahl zwischen 1 und 9 und
        // Konsoleeingabe einlesen.
        System.out.println("Bitte geben sie einen Wert zwischen 1 und 9 ein.");
        zahl = ms.nextInt();


        // TODO: 6. Ermittel Sie  wie oft sich die Zahl in der Liste befindet.
        for (int x : myList) {
            if (x == zahl) {
                zaehleZahl++;
            }
        }


        // TODO: 7. Geben Sie in der Konsole welche Suchzahl und wie oft sich diese Zahl

        System.out.println("\nDie Zahl " + zahl + " kommt " + zaehleZahl + " mal in der Liste vor.\n");

        // TODO: 8. Suchen Sie die Indices, in der die eingegebende Zahl vorkommt und speichern
        // Sie diese in der Liste.
        for (int i = 19; i >= 0; i--) {
            if (myList.get(i) == zahl) {
                myIndices.add(i);
            }
        }

        // TODO: 9. Geben Sie die Gefundene Indices aus.
        // Benutzen Sie eine foreach Schleife.
        for (int x : myIndices) {
            System.out.println("\nDie Zahl kommt an folgenden Indices in der Liste vor: " + x);
        }


        // TODO: 10. L�schen Sie die gesuchte Zahl aus der Liste.
        for (int x : myIndices) {
            myList.remove(x);
        }

        // TODO: 11. Geben Sie die ver�nderte Liste aus.
        // Verwenden Sie eine Z�hlschleife.

        System.out.println("\nListe nach L�schung von " + zahl + ": ");
        for (int x : myList) {
            System.out.println(x);
        }
        // TODO: 12. F�gen Sie hinter jeder 5 eine 0 ein.
        ArrayList<Integer> indicesOfFives = new ArrayList<>();
        for (int x = 0; x < myList.size(); x++) {
            if (myList.get(x) == 5) {
                indicesOfFives.add(x);
            }
        }
        for (int x = 0; x < indicesOfFives.size(); x++){
            myList.add(indicesOfFives.get(x)+x+1, 0);
        }

        // TODO: 13. Geben Sie die ver�nderte Liste erneut aus.
        // Verwenden Sie eine Z�hlschleife.
        System.out.println("\nListe nach Einfuegen von 0 hinter jeder 5");
        for (int x : myList) {
            System.out.println(x);
        }
        ms.close();
    }

}
